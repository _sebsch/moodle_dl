from robobrowser import RoboBrowser


class MoodleBrowser(RoboBrowser):
    def __init__(self, hostname, login, _log=None):
        super(MoodleBrowser, self).__init__(parser='lxml')
        self.log = _log

        self.hostname = hostname
        self.username = login['user']
        self.passwd = login['pass']

        self.login()

    def login(self):
        self.open(self.hostname)

        form = self.get_form(id='login')
        form['username'] = self.username
        form['password'] = self.passwd
        self.submit_form(form)

        if not self.find('span', {'class': 'login'}) is None:
            raise ConnectionError("Probleme mit Login")
