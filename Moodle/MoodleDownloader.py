from urllib import request


class MoodleDownloader:
    def __init__(self, moodle_browser, file_writer, _log):

        self.ignorefiles = ['view.php', 'discuss.php', 'print.php', 'complete.php', 'CoursePage.php', '#']
        self.moodle_browser = moodle_browser
        self.file_writer = file_writer
        self._log = _log

        courses = self.get_class_list()
        content = self.get_content(courses)

        self.download(content)

    def download(self, content):

        for course in content:
            self._log.log_debug("Downloading" , sub_message = f"{course}")


            for link in content[course]:
                self.moodle_browser.open(link)

                content_region = self.moodle_browser.find('section', {"id": "region-main"})

                for file in [(a['href']) for a in content_region.find_all('a', href=True)]:

                    filename = request.url2pathname(file.split('/')[-1])

                    if True in [(igno in filename) for igno in self.ignorefiles]:
                        self._log.log_debug(f"{filename} flagged as igno.", sub_message="Skipping - igno")
                        continue

                    if self.file_writer.file_exists(course, filename):
                        self._log.log_debug(f"{filename}", sub_message="Skipping - exists")
                        continue


                    self._log.log_info(f"Writing file: {filename}", sub_message="new")

                    try:
                        self.moodle_browser.open(file)

                        self.file_writer.writefile(
                            path=course, name=filename,
                            content=self.moodle_browser.response.content)

                        print('   ..done')

                    except Exception as err:
                        self._log.log_error(f"{err}", sub_message=f"{filename}")


    def get_content(self, courses):

        self._log.log_info(f"collecting content links. Please wait..")
        content_map = {}

        for course in courses:
            self.moodle_browser.open(course)

            course_title = self.moodle_browser.find('h1').text
            self._log.log_debug(f"colecting in classroom {course_title}")

            content_map[course_title] = []

            for section in self.moodle_browser.find_all('ul', {'class': 'section'}):
                for activity in section.find_all('li', {'class': 'activity'}):
                    for a in activity.find_all('a', href=True):
                        content_map[course_title].append(a['href'])

        return content_map

    def get_class_list(self):
        self._log.log_info(f"getting Courses. Please wait..")

        courses = [a['href'] for a in
                   self.moodle_browser.find('div', {'id': 'coc-courselist'}).find_all('a', href=True)]
        self._log.log_debug(f"Found {len(courses)} classrooms")

        return courses
