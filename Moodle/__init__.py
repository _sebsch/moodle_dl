from getpass import getpass

from .MoodleBrowser import MoodleBrowser
from .MoodleDownloader import MoodleDownloader


def get_MoodleBrowser(DBGUSER=None, DBGPASS=None, _log=None):
    return MoodleBrowser(
        hostname='https://moodle.htw-berlin.de',
        login={'user': DBGUSER if DBGUSER != None else input('Benutzername: '),
               'pass': DBGPASS if DBGPASS != None else getpass('Passwort: ')},
        _log=_log
    )
