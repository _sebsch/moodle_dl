import os


class FileWriter:

    def __init__(self, path, log):
        self.path = path

    def mkdir(self, path):
        os.makedirs(path)

    def file_exists(self, path, file):
        dir = os.path.join(self.path, path)
        return os.path.exists(os.path.join(dir, file))

    def writefile(self, path, name, content):

        path = os.path.join(self.path, path)
        filepath = os.path.join(path, name)

        if os.path.exists(filepath):
            print("File exists. Skipping!")
            return

        with open(filepath, 'wb') as outfile:
            outfile.write(content)


